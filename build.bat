if not exist %cd%\build mkdir %cd%\build

set cmake_path=%cd%\CMake\bin\cmake.exe
set project_path=%cd%\

:: Set the build folder as current
cd %cd%\build

:: Launch cmake with the default build generator, passing the received command line arguments
%cmake_path% %* %project_path%

pause
