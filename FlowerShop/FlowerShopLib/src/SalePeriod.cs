// enums
public enum SalePeriod : uint
{
    // all months have 30 days and all years have 365 days - we can adapt this later if needed
    DAILY = 1,
    WEEKLY = 7,
    MONTHLY = 30,
    YEARLY = 365
}