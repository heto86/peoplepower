
using System;

public struct Flower : IFlowerShopItem
{
    public Flower(string name, int price)
    {
        Price = price;
        Name = name;
    }

    // properties
    public string Name { get; set; }
    public int Price { get; set; }
}