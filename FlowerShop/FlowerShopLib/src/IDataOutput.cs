public interface IDataOutput
{
    void outputHeader(string header);
    void outputData(string name, int nrSoldItemsInPeriod, int price, int total);
}