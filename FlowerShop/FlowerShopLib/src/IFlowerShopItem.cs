public interface IFlowerShopItem
{
    string Name { get; set; }
    int Price { get; set; }
}