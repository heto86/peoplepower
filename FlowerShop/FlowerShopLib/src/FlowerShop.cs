
using System;
using System.Collections.Generic;
using System.Windows.Controls.Primitives;

public class FlowerShop
{
    // ctor
    public FlowerShop(IDataInput dataInput, IDataOutput dataOutput)
    {
        _dataInput = dataInput;
        _dataOutput = dataOutput;
        _flowers = new Dictionary<string, Flower>();
        _bouquets = new Dictionary<string, Bouquet>();
        _salesFlowerData = new Dictionary<string, double>();
        _salesBouquetData = new Dictionary<string, double>();
        _reportSalePeriod = SalePeriod.DAILY;
    }

    public void writeReport()
    {
        // keep a local copy that can be modified without affecting the original data
        Dictionary<string, double> salesFlowerData = _salesFlowerData;
        // add flowers from daily bouquets sales to daily flower sales
        foreach (KeyValuePair<string, Bouquet> entry in _bouquets)
        {
            foreach (Tuple<Flower, int> tuple in entry.Value.Flowers)
            {
                salesFlowerData[tuple.Item1.Name] += _salesBouquetData[entry.Key] * tuple.Item2;
            }
        }

        // write all info:
        _dataOutput.outputHeader("FLOWERS:");
        writeData(salesFlowerData, _flowers);
        _dataOutput.outputHeader("BOUQUETS:");
        writeData(_salesBouquetData, _bouquets);
    }

    void writeData<T>(Dictionary<string, double> salesData, Dictionary<string, T> items)
            where T : IFlowerShopItem
    {
        foreach (KeyValuePair<string, double> entry in salesData)
        {
            double soldItemInPeriod = entry.Value * (uint)_reportSalePeriod + 0.5;
            int total = (int)soldItemInPeriod * items[entry.Key].Price;
            _dataOutput.outputData(
                entry.Key,                    // name
                (int)soldItemInPeriod,        // soldItemsInPeriod
                items[entry.Key].Price,       // price
                total);                       // total
        }
    }

    public void readInput()
    {
        _flowers = _dataInput.inputFlowerData();
        _bouquets = _dataInput.inputBouquetData();
        _salesFlowerData = _dataInput.inputSalesFlowerData();
        _salesBouquetData = _dataInput.inputSalesBouquetData();
    }

    public void SetSalePeriod(SalePeriod salePeriod)
    {
        _reportSalePeriod = salePeriod;
    }

    // fields
    private Dictionary<string, Flower> _flowers;
    private Dictionary<string, Bouquet> _bouquets;
    private Dictionary<string, double> _salesBouquetData;
    private Dictionary<string, double> _salesFlowerData;
    private SalePeriod _reportSalePeriod; // defaults to DAILY
    private IDataInput _dataInput;
    private IDataOutput _dataOutput;
}