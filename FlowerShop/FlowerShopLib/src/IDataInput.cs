

using System.Collections.Generic;

public interface IDataInput
{
    Dictionary<string, Flower> inputFlowerData();
    Dictionary<string, Bouquet> inputBouquetData();
    Dictionary<string, double> inputSalesBouquetData();
    Dictionary<string, double> inputSalesFlowerData();
}