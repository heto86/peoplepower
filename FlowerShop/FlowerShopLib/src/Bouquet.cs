
using System;
using System.Collections.Generic;

public struct Bouquet : IFlowerShopItem
{
    // ctor
    public Bouquet(string name, int price)
    {
        Price = price;
        Name = name;
        Flowers = new List<Tuple<Flower, int>>();
    }

    // properties
    public string Name { get; set; }
    public int Price { get; set; }
    public List<Tuple<Flower, int>> Flowers { get; set; }
}