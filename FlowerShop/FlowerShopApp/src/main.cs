using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        Controller controller = new Controller();
        controller.run();
    }
}
