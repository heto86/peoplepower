

public class ConsoleDataOutput : IDataOutput
{
    public void outputData(string name, int nrSoldItemsInPeriod, int price, int total)
    {
        writeCellInfo(name);
        writeCellInfo(nrSoldItemsInPeriod.ToString());
        writeCellInfo(price.ToString());
        writeCellInfo(total.ToString());
        System.Console.Write("|\n");
    }

    public void outputHeader(string header)
    {
        System.Console.WriteLine("|-------------------------|-------------------------|-------------------------|-------------------------|");
        writeCellInfo(header);
        writeCellInfo("Sold Items in Period");
        writeCellInfo("Unit Price");
        writeCellInfo("Total");
        System.Console.Write("|\n");
        System.Console.WriteLine("|-------------------------|-------------------------|-------------------------|-------------------------|");
    }

    private void writeCellInfo(string str)
    {
        System.Console.Write("|" + str);
        const int tableCellLength = 25;
        for (int i = tableCellLength - str.Length; i > 0; --i)
        {
            System.Console.Write(" ");
        }
    }
}