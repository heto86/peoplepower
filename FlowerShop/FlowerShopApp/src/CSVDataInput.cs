

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic.FileIO;
using Microsoft.Win32;

public class CSVDataInput : IDataInput
{

    public CSVDataInput()
    {
        csvWasParsed = false;
        _flowers = new Dictionary<string, Flower>();
        _bouquets = new Dictionary<string, Bouquet>();
        _salesFlowerData = new Dictionary<string, double>();
        _salesBouquetData = new Dictionary<string, double>();
    }

    public Dictionary<string, Bouquet> inputBouquetData()
    {
        parseCSV();
        return _bouquets;
    }

    public Dictionary<string, Flower> inputFlowerData()
    {
        parseCSV();
        return _flowers;   
    }

    public Dictionary<string, double> inputSalesBouquetData()
    {
        parseCSV();
        return _salesBouquetData;
    }

    public Dictionary<string, double> inputSalesFlowerData()
    {
        parseCSV();
        return _salesFlowerData;
    }

    private void parseCSV()
    {
        if (csvWasParsed) return;
        csvWasParsed = true;
        using (TextFieldParser parser = new TextFieldParser(@"../input.csv"))
        {
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");
            while (!parser.EndOfData)
            {
                //Processing row
                string[] fields = parser.ReadFields();
                if ("[flower]" == fields[0].ToLower())
                {
                    Flower flower = new Flower(fields[1], Int32.Parse(fields[2]));
                    _flowers[flower.Name] = flower;
                }
                if ("[bouquet]" == fields[0].ToLower())
                {
                    Bouquet bouquet = new Bouquet(fields[1], Int32.Parse(fields[2]));
                    for( int i = 3; i < fields.Length; i += 2) 
                    {
                        bouquet.Flowers.Add(Tuple.Create(_flowers[fields[i]], Int32.Parse(fields[i + 1])));
                    }
                    _bouquets[fields[1]] = bouquet;
                }
                if("[salesflower]" == fields[0].ToLower())
                {
                    SalePeriod period = (SalePeriod)Enum.Parse(typeof(SalePeriod), fields[3], true);
                    _salesFlowerData[_flowers[fields[1]].Name] = Double.Parse(fields[2]) / (uint)period;
                }
                if ("[salesbouquet]" == fields[0].ToLower())
                {
                    SalePeriod period = (SalePeriod)Enum.Parse(typeof(SalePeriod), fields[3], true);
                    _salesBouquetData[_bouquets[fields[1]].Name] = Double.Parse(fields[2]) / (uint)period;
                }

            }
        }
    }

    private bool csvWasParsed;
    private Dictionary<string, Flower> _flowers;
    private Dictionary<string, Bouquet> _bouquets;
    private Dictionary<string, double> _salesBouquetData;
    private Dictionary<string, double> _salesFlowerData;
}