using System;
using System.IO;

class Controller
{
    public void run()
    {
        // read from CSV, we can implement XML/JSON/SQL/AlienWayToStoreData
        IDataInput dataInput = new CSVDataInput();
        // write to Console, we can implement any other way to output the data
        IDataOutput dataOutput = new ConsoleDataOutput();
        FlowerShop shop = new FlowerShop(dataInput, dataOutput);
        string answer = "";
        do
        {
            shop.readInput();
            shop.SetSalePeriod(SalePeriod.MONTHLY);
            shop.writeReport();
            Console.WriteLine("Do you want to run again? yes/no");
            answer = Console.ReadLine();
        }
        while ("yes" == answer.ToLower());
    }
}
