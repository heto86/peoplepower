cmake_minimum_required(VERSION 3.8)
project("PeoplePower" CSharp)

add_subdirectory(FlowerShop/FlowerShopLib)
add_subdirectory(FlowerShop/FlowerShopApp)
