# People Power Interview

## How to use:

Just run the build.bat - it should create the solution for you, based on what IDE you have installed. I've added external CMake as not being the most used application for C# generating solution and to not have any install prerequisite to build the project.

## 1st Assignement - FlowerShop

### Problem Definition:

Folosind limbajul C#, sa se realizeze o aplicatie (poate fi aplicatie de tip consola) pentru o florarie, unde se vand diverse tipuri de flori. Acestea pot fi cumparate la bucata sau sub forma unui buchet.

Buchetele de flori oferite pot fi mari (format din 9 trandafiri, 10 gladiole si 3 orhidee), medii (6 trandafiri si 5 gladiole) sau mici (5 trandafiri). Deocamdata, doar aceste tipuri de buchete sunt oferite de florarie. Pe langa pretul florilor, cand un client cumpara un buchet de flori mai are de plata in plus 2 lei pe asambalare lui.

Stiind ca intr-o saptamana florareasa vinde 100 de buchete de flori (20 mari, 15 medii si 65 mici) si 20 bucati de trandafiri (10 lei/buc), 10 bucati de orhidee (30 lei/buc) si 5 bucati gladiole (15 lei/buc), care ar fi raportul de vanzari pentru o luna de zile?  Afisati acest raport.

### Details:

Input is a .csv file, found in FlowerShop/FlowerShopApp/data. There can be written/added/modified any details about flower/bouquet/sales data. 

## 2nd Assignement - Geometrical Library

### Problem Definition:
Define a geometrical library with the following classes: circle, rectangle and square.
Each element should have:
- Color
- Position
- Identifier which specifies the number of objects of that type instantiated so far
- Other useful properties
Add operations to:
- Move the elements position using a vector
- Scale an element

Create a console application which exposes the following functionality:
1. Create elements and store them in a container
1. Display on the screen the details of the elements created so far
1. Move and scale the elements and print the result (optional)
1. Report intersection points between two elements (optional)